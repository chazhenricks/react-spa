import React, { Component } from "react";
import { Route, NavLink, HashRouter } from "react-router-dom";

import Home from "./Components/Home";
import About from "./Components/About.js";
import Contact from "./Components/Contact";

class App extends Component {
  render() {
    return (
      <HashRouter>
     <div className="App">
        <h1>
          Simple SPA
          <ul>
            <li><NavLink to="/">Home</NavLink></li>
            <li><NavLink to="/about">About</NavLink></li>
            <li><NavLink to="/contact">Contact</NavLink></li>
          </ul>
          <div className="content">
            <Route exact path="/" component={Home} />
            <Route path="/about" component={About} />
            <Route path="/contact" component={Contact} />
          </div>
        </h1>
    </div>
    </HashRouter>
    );
  }
}

export default App;
